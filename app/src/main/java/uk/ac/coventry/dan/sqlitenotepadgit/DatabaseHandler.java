package uk.ac.coventry.dan.sqlitenotepadgit;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dan on 20/03/2015.
 */
public class DatabaseHandler extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    //Assigning the fields in the database
    private static final String DATABASE_NAME = "notePad",
            DATA_TABLE = "notes", KEY_ID = "id", KEY_TITLE = "title", KEY_TEXT = "text";

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    //creates the database with the fields above
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + DATA_TABLE + "(" + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + KEY_TITLE + " TEXT," + KEY_TEXT + " TEXT)");
    }

    //when updating drops the table if it exists and creates a new one
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + DATA_TABLE);

        onCreate(db);
    }

    //creates what will be a note in the notepad
    public void createNotes(notepadView note) {
        SQLiteDatabase db = getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(KEY_TITLE, note.get_title());
        values.put(KEY_TEXT, note.get_text());


        db.insert(DATA_TABLE, null, values);
        db.close();
    }

    public notepadView getNotes(int id) {
        SQLiteDatabase db = getReadableDatabase();

        Cursor cursor = db.query(DATA_TABLE, new String[]{KEY_ID, KEY_TITLE, KEY_TEXT}, KEY_ID + "=?", new String[]{String.valueOf(id)}, null, null, null, null);

        if (cursor != null)
            cursor.moveToFirst();

        notepadView notes = new notepadView(Integer.parseInt(cursor.getString(0)), cursor.getString(1), cursor.getString(2));
        //closing the cursor and database after using them to free up resources
        db.close();
        cursor.close();
        return notes;
    }

    //used when deleting a note
    public void deleteNote(notepadView note) {
        SQLiteDatabase db = getWritableDatabase();
        db.delete(DATA_TABLE, KEY_ID + "=?", new String[]{String.valueOf(note.getId())});
        db.close();
    }

    public int getNoteCount() {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + DATA_TABLE, null);
        int count = cursor.getCount();
        db.close();
        cursor.close();

        return count;
    }

    //updates the affected rows
    public int updateNotes(notepadView note) {
        SQLiteDatabase db = getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(KEY_TITLE, note.get_title());
        values.put(KEY_TEXT, note.get_text());


        int rowsAffected = db.update(DATA_TABLE, values, KEY_ID + "=?", new String[]{String.valueOf(note.getId())});
        db.close();

        return rowsAffected;
    }

    public List<notepadView> getAllNotes() {
        List<notepadView> notes = new ArrayList<notepadView>();

        SQLiteDatabase db = getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + DATA_TABLE, null);

        if (cursor.moveToFirst()) {
            do {
                notes.add(new notepadView(Integer.parseInt(cursor.getString(0)), cursor.getString(1), cursor.getString(2)));
            }
            while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return notes;
    }
}