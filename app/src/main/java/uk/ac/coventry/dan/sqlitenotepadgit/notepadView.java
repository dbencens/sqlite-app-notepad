package uk.ac.coventry.dan.sqlitenotepadgit;

/**
 * Created by Dan on 20/03/2015.
 */
public class notepadView {

    private String _title, _text;

    private int _id;

    public notepadView (int id, String title, String text) {
        _id = id;
        _title = title;
        _text = text;

    }

    public int getId() { return _id; }

    public String get_title() {
        return _title;
    }
    public String get_text() {
        return _text;
    }


}