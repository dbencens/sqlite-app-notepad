package uk.ac.coventry.dan.sqlitenotepadgit;

import android.app.Activity;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends Activity {


    EditText titleTxt, textTxt;
    List<notepadView> Notes = new ArrayList<notepadView>();
    ListView NotepadView;
    int longClick;
    ArrayAdapter<notepadView> notesAdap;
    DatabaseHandler databasehandler;
    private static final int DELETE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//declaring variables
        titleTxt = (EditText) findViewById(R.id.txtTitle);
        textTxt = (EditText) findViewById(R.id.txtNotes);
        NotepadView = (ListView) findViewById(R.id.listView);
        databasehandler = new DatabaseHandler(getApplicationContext());
        final Button addBtn = (Button) findViewById(R.id.btnsave);
//setting up the tabs
        TabHost tabHost = (TabHost) findViewById(R.id.tabHost);

        tabHost.setup();


        TabHost.TabSpec tabSpec = tabHost.newTabSpec("AddNote");
        tabSpec.setContent(R.id.tabaddNote);
        tabSpec.setIndicator("Add Note");
        tabHost.addTab(tabSpec);

        tabSpec = tabHost.newTabSpec("ViewNotes");
        tabSpec.setContent(R.id.tabviewNote);
        tabSpec.setIndicator("Saved");
        tabHost.addTab(tabSpec);

// long click
        registerForContextMenu(NotepadView);
        NotepadView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                longClick = position;

                return false;
            }
        });
//This is what will happen when the button is clicked, this adds a new note to the database
        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                notepadView note = new notepadView(databasehandler.getNoteCount(), String.valueOf(titleTxt.getText()), String.valueOf(textTxt.getText()));
                if (noteExists(note)) {

                    Toast.makeText(getApplicationContext(), " This name already exists", Toast.LENGTH_SHORT).show();
                    //if note title already exists it will not add it again and display a warning
                } else {
                    databasehandler.createNotes(note);
                    notesAdap.notifyDataSetChanged();
                    Notes.add(note);
                    Toast.makeText(getApplicationContext(), "Notes Added", Toast.LENGTH_SHORT).show();
                    Toast.makeText(getApplicationContext(), String.valueOf(titleTxt.getText()) + " GoT ReKT", Toast.LENGTH_SHORT).show();

                    return;

                }
            }
        });
        titleTxt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {}

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                addBtn.setEnabled(String.valueOf(titleTxt.getText()).trim().length() > 0);}

            @Override
            public void afterTextChanged(Editable editable) {}
        });

//if note count is not = to 0 it adds all the notes and repopulates the list
        if (databasehandler.getNoteCount() != 0)
            Notes.addAll(databasehandler.getAllNotes());
        populateList();
    }
    //remove user on long click setup
    public void onCreateContextMenu(ContextMenu menu, View view, ContextMenu.ContextMenuInfo info){
        super.onCreateContextMenu(menu, view, info);
        menu.setHeaderTitle("Options");
        menu.add(menu.NONE, DELETE, menu.NONE, "Delete");

    }
    public boolean onContextItemSelected(MenuItem item){
        switch (item.getItemId()){
            case DELETE:
                databasehandler.deleteNote(Notes.get(longClick));
                Notes.remove(longClick);
                notesAdap.notifyDataSetChanged();
                Toast.makeText(getApplicationContext(), "Deleted", Toast.LENGTH_SHORT).show();
                break;

        }
        return super.onContextItemSelected(item);

    }
    private boolean noteExists(notepadView notes) {
        String name = notes.get_title();
        int noteCount = Notes.size();

        for (int i = 0; i < noteCount; i++) {
            if (name.compareToIgnoreCase(Notes.get(i).get_title()) == 0)
                return true;
        }
        return false;
    }

    private void populateList() {
        notesAdap = new NotesListAdapter();
        NotepadView.setAdapter(notesAdap);
    }

    private class NotesListAdapter extends ArrayAdapter<notepadView> {
        public NotesListAdapter() {
            super (MainActivity.this, R.layout.list_item, Notes);
        }

        @Override
        public View getView(int position, View view, ViewGroup parent) {
            if (view == null)
                view = getLayoutInflater().inflate(R.layout.list_item, parent, false);

            notepadView currentNote = Notes.get(position);

            TextView title = (TextView) view.findViewById(R.id.titleField);
            title.setText(currentNote.get_title());
            TextView notes = (TextView) view.findViewById(R.id.noteField);
            notes.setText(currentNote.get_text());



            return view;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

}

